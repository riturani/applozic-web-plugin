var ENV_URL= "https://apps.applozic.com";

document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("login-submit").onclick = function() {
        getUserData();
    };
    document.getElementById("logout").onclick = function() {
        logout();
    };

    var loginPassword = document.getElementById("loginPassword");
    loginPassword.addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            document.getElementById("login-submit").click();
        }
    });
});

function getUserData() { //loadScript();
    //pass user login credentials to chat plugin script
    var appId = document.getElementById("applicationId").value;
    var appIdd = document.getElementById("appIdd").value;
    var channel = document.getElementById("channel").value;
    var userId = document.getElementById("loginId").value;
    var pass = document.getElementById("loginPassword").value;
    var videoSource = document.getElementById("videoSource").value;
    var audioSource = document.getElementById("audioSource").value;
    intializeChat(appId, userId, pass, appIdd, channel, videoSource, audioSource);
}

function logout() {
    $applozic.fn.applozic('logout');
    location.reload();
    /*document.getElementById("login-form").classList.remove('n-vis');
    document.getElementById("login-form").classList.add('vis');
    document.getElementById("success-text").classList.add('n-vis');
    document.getElementById("success-text").classList.remove('vis');*/
}

 //this method logs in a user.

function intializeChat(appId, userId, pass, appIdd, channel, videoSource, audioSource) {

    var contactsJSON = [{
        "userId": "bhupesh",
        "displayName": "Applozic",
        "imageLink": "https://www.applozic.com/resources/images/applozic_icon.png", // image url (optional)
        "imageData": "Base64 encoded image data" // or image data (optional)
    }];


  (function(d, m){var s, h;
  s = document.createElement("script");
  s.type = "text/javascript";
  s.async=true;
  s.src="https://apps.applozic.com/sidebox.app";
  h=document.getElementsByTagName('head')[0];
  h.appendChild(s);
  window.applozic=m;
  m.init=function(t){m._globals=t;}})(document, window.applozic || {});

    window.applozic.init({
        baseUrl : ENV_URL,
        appId: appId, //Get your application key from https://www.applozic.com
        userId: userId, //Logged in user's id, a unique identifier for user
        userName: '', //User's display name
        imageLink: '', //User's profile picture url
        email: '', //optional
        contactNumber: '', //optional, pass with internationl code eg: +13109097458
        desktopNotification: true,
        source: '1', // optional, WEB(1),DESKTOP_BROWSER(5), MOBILE_BROWSER(6)
        notificationIconLink: 'https://www.applozic.com/favicon.ico', //Icon to show in desktop notification, replace with your icon
        authenticationTypeId: 1, //1 for password verification from Applozic server and 0 for access Token verification from your server
        accessToken: pass, //optional, leave it blank for testing purpose, read this if you want to add additional security by verifying password from your server https://www.applozic.com/docs/configuration.html#access-token-url
        locShare: true,
        googleApiKey: "AIzaSyDKfWHzu9X7Z2hByeW4RRFJrD9SizOzZt4", // your project google api key
        video: false,
        googleMapScriptLoaded: false, // true if your app already loaded google maps script
        //   mapStaticAPIkey: "AIzaSyCWRScTDtbt8tlXDr6hiceCsU83aS2UuZw",
        autoTypeSearchEnabled: true, // set to false if you don't want to allow sending message to user who is not in the contact list
        loadOwnContacts: true, //set to true if you want to populate your own contact list (see Step 4 for reference)
        olStatus: false, //set to true for displaying a green dot in chat screen for users who are online
        onInit: function(response) {
            if (response === "success") {

                document.getElementById("login-form").classList.add('n-vis');
                document.getElementById("success-text").classList.add('vis');
                document.getElementById("success-text").classList.remove('n-vis');

                $applozic.fn.applozic('loadTab', '');
                $applozic.fn.applozic('loadContacts', { "contacts": contactsJSON });
                document.getElementsByClassName("mck-videocall-btn")[0].classList.remove('n-vis')
                document.getElementById("mck-btn-video-call").addEventListener("click", function()
                 { 
                    document.getElementById("mck-btn-video-call").disabled = true;
                  //document.getElementById("join").disabled = true;
                  //document.getElementById("video").disabled = true;
                  var channel_key = null;

                  console.log("Init AgoraRTC client with App ID: " + appIdd);
                  client = AgoraRTC.createClient({mode: 'interop'});
                  client.init(appIdd, function () {
                    console.log("AgoraRTC client initialized");
                    client.join(channel_key, channel, null, function(uid) {
                      console.log("User " + uid + " join channel successfully");
                      if (document.getElementById("video").checked){
                        camera = videoSource;
                        microphone = audioSource;
                        localStream = AgoraRTC.createStream({streamID: uid, audio: true, cameraId: camera, microphoneId: microphone, video: document.getElementById("video").checked, screen: false});
                        //localStream = AgoraRTC.createStream({streamID: uid, audio: false, cameraId: camera, microphoneId: microphone, video: false, screen: true, extensionId: 'minllpmhdgpndnkomcoccfekfegnlikg'});
                        if (document.getElementById("video").checked) {
                          localStream.setVideoProfile('720p_3');

                        }

                        // The user has granted access to the camera and mic.
                        localStream.on("accessAllowed", function() {
                          console.log("accessAllowed");
                        });

                        // The user has denied access to the camera and mic.
                        localStream.on("accessDenied", function() {
                          console.log("accessDenied");
                        });

                        localStream.init(function() {
                          console.log("getUserMedia successfully");
                          localStream.play('agora_local');

                          client.publish(localStream, function (err) {
                            console.log("Publish local stream error: " + err);
                          });

                          client.on('stream-published', function (evt) {
                            console.log("Publish local stream successfully");
                          });
                        }, function (err) {
                          console.log("getUserMedia failed", err);
                        });
                      }
                    }, function(err) {
                      console.log("Join channel failed", err);
                    });
                  }, function (err) {
                    console.log("AgoraRTC client init failed", err);
                  });

                  channelKey = "";
                  client.on('error', function(err) {
                    console.log("Got error msg:", err.reason);
                    if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
                      client.renewChannelKey(channelKey, function(){
                        console.log("Renew channel key successfully");
                      }, function(err){
                        console.log("Renew channel key failed: ", err);
                      });
                    }
                  });


                  client.on('stream-added', function (evt) {
                    var stream = evt.stream;
                    console.log("New stream added: " + stream.getId());
                    console.log("Subscribe ", stream);
                    client.subscribe(stream, function (err) {
                      console.log("Subscribe stream failed", err);
                    });
                  });

                  client.on('stream-subscribed', function (evt) {
                    var stream = evt.stream;
                    console.log("Subscribe remote stream successfully: " + stream.getId());
                    if ($('div#video #agora_remote'+stream.getId()).length === 0) {
                      $('div#video').append('<div class="col-md-6 col-xs-12"><div id="agora_remote'+stream.getId()+'" style="float:left; width:810px;height:607px;display:inline-block;position:fixed;left: 0;bottom: 0;"></div></div>');
                    }
                    stream.play('agora_remote' + stream.getId());
                  });

                  client.on('stream-removed', function (evt) {
                    var stream = evt.stream;
                    stream.stop();
                    $('#agora_remote' + stream.getId()).remove();
                    console.log("Remote stream is removed " + stream.getId());
                  });

                  client.on('peer-leave', function (evt) {
                    var stream = evt.stream;
                    if (stream) {
                      stream.stop();
                      $('#agora_remote' + stream.getId()).remove();
                      console.log(evt.uid + " leaved from this channel");
                    }
                  });


                 });
                // login successful, perform your actions if any, for example: load contacts, getting unread message count, etc
            } else {
                // error in user login/register (you can hide chat button or refresh page)
            }
        },
        contactDisplayName: function(otherUserId) {
            //return the display name of the user from your application code based on userId.
            return "";
        },
        contactDisplayImage: function(otherUserId) {
            //return the display image url of the user from your application code based on userId.
            return "";
        },
        onTabClicked: function(response) {
            // write your logic to execute task on tab load
            //   object response =  {
            //    tabId : userId or groupId,
            //    isGroup : 'tab is group or not'
            //  }
        }
    });
}